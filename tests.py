#!/usr/bin/python3
# -*-coding:Utf-8 -*
"""Testing module"""

import unittest
import main

def test():
    """Run the tests"""
    unittest.main()

class MainTest(unittest.TestCase):
    """Test the main module"""

    def test_get_ip(self):
        """Test the function main.get_ip(domain, port, all_ip=False)"""

        ips = main.get_ip("localhost", 80, False)
        correct_ips = ["127.0.0.1"]

        if not len(ips) == len(correct_ips):
            self.fail("Number of IPs does not match IPs : " + str(ips))
        else:
            for i, ip in enumerate(ips):
                if ip != correct_ips[i]:
                    self.fail("Invalid IP : " + str(ips))

    def test_get_ips(self):
        """Test the function main.get_ip(domain, port, all_ip=True)"""

        ips = main.get_ip("localhost", 80, True)
        correct_ips = ["127.0.0.1", "127.0.0.1", "127.0.0.1"]

        if not len(ips) == len(correct_ips):
            self.fail("Number of IPs does not match IPs : " + str(ips))
        else:
            for i, ip in enumerate(ips):
                if ip != correct_ips[i]:
                    self.fail("Invalid IP : " + str(ips))

if __name__ == '__main__':
    test()