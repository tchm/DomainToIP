#!/usr/bin/python3
# -*-coding:Utf-8 -*
"""Main module of DomainToIP"""

import socket
import argparse
import tests

VERSION = "1.1"

def main():
    """Main function of DomainToIP"""

    print("DomainToIP version", VERSION)

    arg_parser = argparse.ArgumentParser()

    arg_parser.add_argument("domain", help="Domain to convert to IP")
    arg_parser.add_argument("-p", "--port", default=80, type=int, help="Port used to contact distant server")
    arg_parser.add_argument("-a", "--allIp", default=False, action="store_true", help="List all IPs, not only ones can be used in TCP (can duplicate the same IP)")
    arg_parser.add_argument("-t", "--test", default=False, action="store_true", help="Run the tests (dev)")

    args = arg_parser.parse_args()
    if args.test:
        tests.test()
    else:
        run(args.domain, args.port, args.allIp)

def run(domain, port, all_ip):
    """Make the test"""

    print("Connecting to", domain, "port", port)
    ip = get_ip(domain, port, all_ip)

    print("\nIPs :")

    for i in ip:
        print(i)


def get_ip(domain, port, all_ip):
    """Get the IPs"""
    ip = socket.getaddrinfo(domain, port)
    ips = list()

    i = 0
    while i < len(ip):
        current_ip = ip[i]

        if not all_ip:
            if current_ip[1] == socket.SOCK_STREAM:
                ips.append(current_ip[4][0])
        else:
            ips.append(current_ip[4][0])

        i += 1
    return ips

if __name__ == '__main__':
    main()
