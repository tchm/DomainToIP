# DomainToIP

This Python 3 script can find the IP of a domain.

*Exemple :*
>`main.py google.com`

-------------------------
You can give some args to the script :
* `-p PORT` or `--port PORT` where PORT is a number : use this to use a port for making the request. *Default: 80*
* `-a` or `--allIp` list all IPs the Python API can find with all protocols. May duplicate IPs.
* `-t` or `--test` : run unittest but **seem to not work.**